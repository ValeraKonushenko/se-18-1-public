#pragma once
namespace ASD {
	template <class T>
	class Array
	{
		public:
			T *ar;
			unsigned int count, size, grow;
		public:
			Array(int s = 3, int g = 20) { ar = 0; count = size = 0;  grow = g; SetSize(s); }
			~Array() { SetSize(0); }
			int GetCount() { return count; }

			T & operator [](unsigned int i) {
				if (i >= count) throw 2;
				return ar[i];
			}
			Array & RemoveAll() { count = 0; return *this; }
			Array & FreeExtra() { SetSize(count); return *this; }
			Array & SetSize(unsigned int s) {
				if (s == 0) { 
					if (ar) delete[]ar;
					ar = 0;
					count = 0; size = 0;
					return *this;
				}
				T *ar1 = new T[s];
				if (ar) {
					for (unsigned int i = 0; i < count && i < s; i++) {
						ar1[i] = ar[i];
					}
					delete[]ar;
				}
				ar = ar1;
				if (count > s) count = s;
				size = s;
				return *this;
			}

			int Add(T v) {				
				if (count >= size) {
					SetSize(size + grow);
				}
				ar[count++] = v;
				return count - 1;
			}		

			void RemoveAt(unsigned int pos) {
				if (pos >= GetCount()) throw 3;
				for (unsigned int i = pos; i < GetCount(); i++) {
					ar[i] = ar[i + 1];
				}
				count--;
			}
	};
}
