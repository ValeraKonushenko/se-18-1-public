#pragma once
namespace ASD {
	template <class T>
	class List
	{
		protected:
			class Node {
				public:
					T data;
					Node *prev,*next;
				public:
					Node(T d, Node *p, Node *n = 0) : data(d) {
						next = n; prev = p;
					}

					Node * Get() { return this; }
			};
			Node  * head, *tail;
			int count;
		public:

			typedef Node * POS;
			List() { head = tail = 0; count = 0; }
			~List() { RemoveAll(); }

			int GetCount() { return count; }

			T GetNext(POS &t) {
				T data = ((Node *)t)->data;
				t = (POS)((Node *)t)->next;
				return data;
			}
			T GetPrev(POS &t) {
				T data = ((Node *)t)->data;
				t = (POS)((Node *)t)->prev;
				return data;
			}
			POS GetHead() { return (POS)head; }
			POS GetTail() { return (POS)tail; }

			void RemoveAll() {
				Node * t = head;
				while (t) {
					Node * m = t;
					t = t->next;
					delete m;
				}
			}

			List & AddTail(T v) {
				Node * node = new Node(v, tail);
				if (tail) tail->next = node;
				if (!head) head = node;
				tail = node;
				count++;
				return *this;
			}

			List & AddHead(T v) {
				Node * node = new Node(v, 0, head);				
				if (head) head->prev = node;
				if (!tail) tail = node;				
				head = node;
				count++;
				return *this;
			}
	};
}

