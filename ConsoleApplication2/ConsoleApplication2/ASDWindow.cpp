#include "pch.h"
#include "ASDWindow.h"


ASD::Window::Window() {
	x = y = 0;
	w = GetCols() ;
	h = GetRows() - 1;
	color = White;
	bgcolor = LightBlue;
	bordercolor = Red;
	buf[0] = 218;
	buf[1] = 196;
	buf[2] = 191;
	buf[3] = 179;
	buf[4] = 192;
	buf[5] = 217;
}

void ASD::Window::Show() {
	int i, j;
	GotoXY(x, y);
	::SetColor(bordercolor, bgcolor);
	std::cout << buf[0];
	for (i = 0; i < w - 2; i++) {
		std::cout << buf[1];
	}
	std::cout << buf[2];

	for (j = 0; j < h - 2; j++) {
		GotoXY(x, y + j + 1);
		::SetColor(bordercolor, bgcolor);
		std::cout << buf[3];
		::SetColor(bgcolor, bgcolor);
		for (i = 0; i < w - 2; i++) {
			std::cout << " ";
		}
		::SetColor(bordercolor, bgcolor);
		std::cout << buf[3];
	}
	::SetColor(bordercolor, bgcolor);
	GotoXY(x, y + h - 1);
	std::cout << buf[4];
	for (i = 0; i < w - 2; i++) {
		std::cout << buf[1];
	}
	std::cout << buf[5];
	GotoXY(x + 1, y + 1);
	::SetColor(color, bgcolor);
	Draw();
}
