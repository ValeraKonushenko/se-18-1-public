#pragma once
#include "ASDString.h"
#include <conio.h>
#include "fun_console.h"

namespace ASD {
	class Input
	{
		int x, y, len;
		String str;
		public:
			Input() { x = y = 0; len = 10; }
			void SetPosition(int x_, int y_)  { x = x_; y = y_; }
			void SetLength(int len_) { len = len_; }
			String get() { return str; }
			bool Read() {
				int pos = str.length();
				int oldlen = pos;
				while (1) {
					GotoXY(x, y);
					if (oldlen > str.length()) {
						for (int i = 0; i < oldlen; i++) {
							std::cout << " ";
						}
					}
					oldlen = str.length();
					GotoXY(x, y);
					std::cout << str;
					GotoXY(x + pos, y);
					int key = _getch();
					if (key == 224) {
						key = _getch();

						if (key == 75 && pos) {//left
							pos--;
						}
						else if (key == 77 && pos < str.length()) {//right
							pos++;
						}
						else if (key == 83 && pos < str.length()) {//del
							str.RemoveAt((pos == str.length() - 1)?pos:(pos+1));
						}
						continue;
					}

					if (key == 27) {
						str = ""; return false;
					}
					if (key == 13) break;
					if (key == 8 && pos) {//backspace
						str.RemoveAt(--pos);
					}
					if (key >= 32 && (!len || str.length() < len)) {
						str.InsertAt(pos, (char)key);
						pos++;
					}
				}
				return true;
			}
	};
}
