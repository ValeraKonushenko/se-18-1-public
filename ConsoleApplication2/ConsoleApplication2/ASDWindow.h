#pragma once
#include "fun_console.h"
#include "ASDString.h"
#include "ASDArray.h"
#include <iostream>
#include <conio.h>
#include <io.h>

namespace ASD {
	class Window
	{
		protected:
			unsigned int color, bgcolor, x, y, w, h, bordercolor;
			unsigned char buf[6];
		public:
			enum {
				left = 1,
				center = 2,
				right = 4,
				top = 8,
				middle = 16,
				bottom = 32
			};
			Window();
			void SetColor(int c) { color = c % 16; }
			void SetBgColor(int c) { bgcolor = c % 16; }
			void SetBorderColor(int c) { bordercolor = c % 16; }
			void Move(int x_, int y_, int w_, int h_) {
				x = x_;
				y = y_;
				w = w_;
				h = h_;
			}
			void Show();
			virtual void Draw() {}

			unsigned int GetClientWidth() { return w - GetBorderWidth()*2; }
			unsigned int GetClientHeight() { return h - GetBorderHeight()*2; }
			unsigned int GetBorderWidth() { return 1; }
			unsigned int GetBorderHeight() { return 1; }

			void Write(unsigned int x_, unsigned int y_, const char *buf) {
				if (x_ >= GetClientWidth() || y_ >= GetClientHeight()) return;
				GotoXY(x + x_ + GetBorderWidth(), y + y_ + GetBorderHeight());
				int i = 0;
				while (buf[i] && x_ + i < GetClientWidth()) {
					std::cout << buf[i];
					i++;
				}
				std::cout << std::flush;
			}

			void SetAlign(char align) {
				if ((align & center) && w < ::GetCols()) {
					x = (::GetCols() - w) / 2;
				}
				else if ((align & right) && w < ::GetCols()) {
					x = (::GetCols() - w);
				}
				else if (align & left) {
					x = 0;
				}

				if ((align & middle) && h < ::GetRows()) {
					y = (::GetRows() - h) / 2;
				}
				else if ((align & bottom) && w < ::GetRows()) {
					y = (::GetRows() - h);
				}
				else if (align & top) {
					y = 0;
				}
			}
	};

	class Message: public Window {
		ASD::String text;
		int align;
		public:
			Message() : Window() {

			}

			void SetText(ASD::String t) { text = t; }
			void SetAlignText(char a) { align = a; }
			void Draw() {

				//std::cout << text;
				//_getch();
			}
	};

	class Menu :public Window {
		protected:
			int select;
			unsigned int first;
			unsigned int selectedColor;
			unsigned int selectedBgColor;

		public:
			Menu() : Window() {
				select = 0;
				selectedColor = White;
				selectedBgColor = Red;
			}
			virtual void oninit(){}
			virtual bool onkeydown(int) { return false; }
			class DrawItemParams {
			public:
				int x, y, row;
			};
			virtual void ondrawitem(DrawItemParams &params) = 0;
			virtual unsigned int GetCount() = 0;

			int DoModal() {
				first = 0;
				oninit();
				while (1) {
					//
					if ((unsigned int)select > first + GetClientHeight() - 1) {
						first = (unsigned int)select - GetClientHeight() + 1;
					}
					else if ((unsigned int)select < first) {
						first = (unsigned int)select;
					}
					//
					Show();
					int key = _getch();
					//
					if (key == 224) {
						key = _getch();
						if (onkeydown(key)) continue;
						if (key == 80) {							
							select++;
							if (select == GetCount()) select = 0;
						}
						if (key == 72 && select > 0) {
							select--;
						}
						continue;
					}
					//
					if (onkeydown(key)) continue;
					if (key == 27) {
						select = -1;
						break;
					}
					if (key == 13) {
						break;
					}
					
				}
				return select;
			}


			void Draw() {
				DrawItemParams params;
				for (unsigned int i = first, y = 0; i < GetCount() && y < GetClientHeight(); i++, y++) {
					if (i == select) {
						::SetColor(selectedColor, selectedBgColor);
					}
					else {
						::SetColor(color, bgcolor);
					}
					params.x = x + 1;
					params.y = this->y + y + 1;
					params.row = i;
					ondrawitem(params);
				}
			}
	};

	class ArrayMenu :public Menu {
	protected:
		Array<String> items;
	public:
		ArrayMenu() : Menu() {

		}

		void Add(String s) {
			items.Add(s);
		}

		virtual void ondrawitem(DrawItemParams &params) {
			::GotoXY(params.x, params.y);
			for (int j = 0; j < GetClientWidth(); j++) {
				
				if (j < items[params.row].length()) {
					std::cout << items[params.row][j];
				}
				else {
					std::cout << " ";
				}
			}
		}

		virtual unsigned int GetCount() {
			return items.GetCount();
		}
	};


	class ColorMenu :public Menu {
	public:
		ColorMenu() : Menu() {

		}

		virtual void ondrawitem(DrawItemParams &params) {
			::SetColor(color, bgcolor);
			::GotoXY(params.x, params.y);
			if (params.row == select) {
				std::cout << ">";
			}
			else {
				std::cout << " ";
			}
			::GotoXY(params.x + GetClientWidth() - 1, params.y);
			if (params.row == select) {
				std::cout << "<";
			}
			else {
				std::cout << " ";
			}
			::GotoXY(params.x + 1, params.y);
			for (int j = 1; j < GetClientWidth() - 1; j++) {
				::SetColor(params.row, params.row);
				std::cout << " ";
			}

		}

		virtual unsigned int GetCount() {
			return 16;
		}
	};

	
	class FilesMenu :public ArrayMenu {
		String curdir;
		String selectedFile;
		int countdir;
	public:
		FilesMenu() : ArrayMenu() {
		}

		String GetSelectedFile() {
			return selectedFile;
		}

		void Fill() {
			countdir = 0;
			items.RemoveAll();
			String tmp(curdir);
			tmp += "\\*.*";
			_finddata_t fileinfo;
			intptr_t list = _findfirst(tmp, &fileinfo);
			while (1) {
				if (strcmp(fileinfo.name, ".") && fileinfo.attrib & _A_SUBDIR) {
					items.Add(fileinfo.name);
					countdir++;
				}
				if (_findnext(list, &fileinfo) < 0) break;
			}
			_findclose(list);
			list = _findfirst(tmp, &fileinfo);
			while (1) {
				if (!(fileinfo.attrib & _A_SUBDIR)) {
					items.Add(fileinfo.name);
				}
				if (_findnext(list, &fileinfo) < 0) break;
			}
			_findclose(list);
		}

		void oninit() {
			char buf[MAX_PATH + 1];
			GetCurrentDirectoryA(MAX_PATH, buf);
			curdir = buf;
			Fill();
		}

		bool onkeydown(int key) {
			if (key == 13) {
				if (select >= countdir) {
					selectedFile = curdir + "\\" + items[select];
					return false;
				}
				if (items[select] == "..") {
					for (int i = curdir.length(); i >= 0; i--) {
						if (curdir[i] == '\\') {
							curdir[i] = 0;
							break;
						}
					}
					select = 0;
					Fill();
					return true;
				}
				else {
					select = 0;
					curdir += "\\" + items[select];
					Fill();
					return true;
				}
				
			}
			return false;
		}

		
		int DoModal1() {	
			int m = -1;
			int countdir;
			char buf[MAX_PATH + 1];
			GetCurrentDirectoryA(MAX_PATH, buf);
			curdir = buf;			
			while (1) {
				select = 0;
				countdir = 0;
				items.RemoveAll();
				String tmp(curdir);
				tmp += "\\*.*";
			
				_finddata_t fileinfo;
				intptr_t list = _findfirst(tmp, &fileinfo);			
				while (1) {
					if (strcmp(fileinfo.name, ".") && fileinfo.attrib & _A_SUBDIR) {
						items.Add(fileinfo.name);
						countdir++;
					}
					if (_findnext(list, &fileinfo) < 0) break;
				}
				_findclose(list);
				list = _findfirst(tmp, &fileinfo);
				while (1) {
					if (!(fileinfo.attrib & _A_SUBDIR)) {
						items.Add(fileinfo.name);
					}
					if (_findnext(list, &fileinfo) < 0) break;
				}
				_findclose(list);
				m = Menu::DoModal();
				if (m == -1) break;
				if (m >= countdir) {
					selectedFile = curdir + "\\" + items[m];
					return m;
				}			
				if (items[m] == "..") {
					for (int i = curdir.length(); i >= 0; i--) {
						if (curdir[i] == '\\') {
							curdir[i] = 0;
							break;
						}
					}
				}
				else {
					curdir += "\\" + items[m];
				}

			}
			return m;
		}
		
	
	};
	
}

